import React from "react";
import { Switch, Route } from "react-router-dom";

const Main = React.lazy(() => import("../main"));
const Blotter = React.lazy(() => import("../sheets/blotter"));
const DMA = React.lazy(() => import("../sheets/dma"));

const Routes = () => (
  <Switch>
    <Route path="/" exact component={Main} />
    <Route path="/blotter" component={Blotter} />
    <Route path="/dma" component={DMA} />
  </Switch>
);

export default Routes;
