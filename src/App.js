import React, { Suspense } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import Routes from "./routes";
import { UserContextProvider } from "./contexts/userContext";

import "./app.css";

const App = () => {
  return (
    <Suspense fallback={<div>Loading...</div>}>
      <Router>
        <UserContextProvider>
          <Routes />
        </UserContextProvider>
      </Router>
    </Suspense>
  );
};

export default App;
