import React, { useContext } from "react";
import ErrorBoundary from "../components/errorBoundary";
import { UserContext } from "../contexts/userContext";

const Blotter = () => {
  const user = useContext(UserContext);

  user.fetchUserData();

  const ErrorComponent = () => {
    new Error("I crashed!");
  };

  return (
    <div>
      <ErrorBoundary component="Blotter">
        <ErrorComponent />
      </ErrorBoundary>
      Blotter
    </div>
  );
};

export default Blotter;
