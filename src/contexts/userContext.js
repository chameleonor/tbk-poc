import React, { createContext, useCallback } from "react";

const userContextData = {
  name: "",
  fetchUserData: () => {},
};

const UserContext = createContext(userContextData);

const UserContextProvider = ({ children }) => {
  const fetchUserData = useCallback(async () => {
    console.log("fetchUserData");
  }, []);

  return (
    <UserContext.Provider value={{ name: "Guilherme Or", fetchUserData }}>
      {children}
    </UserContext.Provider>
  );
};

export { UserContext, UserContextProvider };
